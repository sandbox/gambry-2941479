Watson Conversation
-------------------

This module integrates Chatbot API with Watson Conversation, providing a plugin
to push Drupal content as Watson entities.
